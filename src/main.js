import {createApp} from 'vue'
import HeyUI from 'heyui.ext'
import './style.css'
import 'heyui.ext/themes/index.css'
import App from './App.vue'
import router from "./router.js";

createApp(App).use(router).use(HeyUI).mount('#app')
