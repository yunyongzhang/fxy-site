import {createRouter, createWebHistory} from "vue-router";
import Index from "./components/Index.vue";
import About from "./components/About.vue";
import ChainCatering from "./components/trade/ChainCatering.vue";
import ChainRetail from "./components/trade/ChainRetail.vue";
import AccountingCompany from "./components/trade/AccountingCompany.vue";

import AutomateReconciliation from "@components/product/AutomateReconciliation.vue";
import AutomateAccounting from "@components/product/AutomateAccounting.vue";
import AutomateGenerateVouchers from "@components/product/AutomateGenerateVouchers.vue";
import RealTimeReporting from "@components/product/RealTimeReporting.vue";
import AutomateBank from "@components/product/AutomateBank.vue";
import Finacial from "@components/product/Finacial.vue";

import VIP from "./components/VIP.vue";

export default createRouter({
	history: createWebHistory(),
	routes: [
		{
			path: '/',
			name: 'Index',
			component: Index
		},
		{
			path: '/about',
			name: 'About',
			component: About
		},
		{
			path: '/vip',
			name: 'VIP',
			component: VIP
		},
		{
			path: '/automateAccounting',
			name: 'AutomateAccounting',
			component: AutomateAccounting
		},
		{
			path: '/automateGenerateVouchers',
			name: 'AutomateGenerateVouchers',
			component: AutomateGenerateVouchers
		},{
			path: '/realTimeReporting',
			name: 'RealTimeReporting',
			component: RealTimeReporting
		},{
			path: '/finacial',
			name: 'Finacial',
			component: Finacial
		},{
			path: '/automateReconciliation',
			name: 'AutomateReconciliation',
			component: AutomateReconciliation
		},{
			path: '/automateBank',
			name: 'AutomateBank',
			component: AutomateBank
		},{
			path: '/chainCatering',
			name: 'ChainCatering',
			component: ChainCatering
		},{
			path: '/chainRetail',
			name: 'ChainRetail',
			component: ChainRetail
		},{
			path: '/accountingCompany',
			name: 'AccountingCompany',
			component: AccountingCompany
		}
	]
})
